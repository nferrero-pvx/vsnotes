---
tags:
	- style guide
	- code
---

# Testing

## Using describe

### Top-level describe blocks

When describing a component, use the exact component name in the describe function:

🛑 BAD

```JavaScript
describe('Sliding Panel', () => { ... })
```

🛑 BAD

```JavaScript
describe('SlidingPanel Unit Tests', () => { ... })
```

🛑 BAD

```JavaScript
describe('components/SlidingPanel', () => { ... })
```

✅ GOOD

```JavaScript
describe('SlidingPanel', () => { ... })
```

### Nested describe blocks

Use an additional describe block for each unique configuration you'd like to test:

🛑 BAD

```JavaScript
describe('SlidingPanel', () => {
	describe('does a thing', () => {
		it('as expected', () => {
			expect(true).toBe(true);
		})
	})
})
```

Bad (ambiguous) sentence: `SlidingPanel ‣ does a thing ‣ as expected`

✅ GOOD

```JavaScript
describe('SlidingPanel', () => {
	describe('with XYZ configuration', () => {
		it('has attribute ABC', () => {
			expect(true).toBe(true);
		})
	})
})
```

Good (imperative) sentence: `SlidingPanel ‣ with XYZ configuration ‣ has attribute ABC`
